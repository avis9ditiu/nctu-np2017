#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <deque>
#include <fstream>
#include <map>
#include <array>

#define MAX_OUTPUT_SIZE 100000
#define MAX_DEQUE_SIZE  1000
#define MAX_LINE_SIZE   1000
#define MAX_USER_NUM    30
using namespace std;

string welcome
{"****************************************\n"
 "** Welcome to the information server. **\n"
 "****************************************\n"};
string prompt = "% ";
vector<string> bin_cmd = {"ls", "cat", "removetag", "removetag0", "number", "noop", "delayedremovetag"};
struct User
{
    int id      = 0;
    string name = "(no name)";
    string ip   = "CGILAB";
    int  port   = 511;
    array<string, MAX_USER_NUM> buffer = {};
    deque<string> output_deque;
    map<string, string> env;
};
User user[MAX_USER_NUM];
int sockfd;
int maxfd;
fd_set master;
fd_set read_fds;

string replace_all(string s, string&& from, string&& to)
{
    for(size_t pos = 0; (pos = s.find(from, pos)) != string::npos; )
    {
	s.replace(pos, from.length(), to);
	pos += to.length();
    }
    return s;
}

bool is_unknown(string& cmd)
{
    return find(bin_cmd.begin(), bin_cmd.end(), cmd) == bin_cmd.end();
}

bool is_used(string& name, int request)
{
    for (int i = 0; i < MAX_USER_NUM; i++)
	if (user[i].id > 0 && i != request && user[i].name == name)
	    return true;
    return false;
}

char** get_argv(vector<string>& cmd)
{
    auto argv = new const char*[cmd.size() + 1];
    for (int i = 0; i < cmd.size(); i++)
	argv[i] = cmd[i].c_str();
    argv[cmd.size()] = nullptr;
    return (char**)argv;
}

string exec_pipe(int sockfd, vector<string>& cmd, istringstream& iss, int pipenum, int& inid)
{
    for (const auto& env : user[sockfd - 4].env)
	setenv(env.first.c_str(), env.second.c_str(), 1);
    int errfd = dup(STDERR_FILENO);
    dup2(sockfd, STDERR_FILENO);

    int fd1[2],fd2[2];
    pipe(fd1);
    pipe(fd2);
    if (fork() == 0)
    {
	close(fd1[1]);
	close(fd2[0]);
	dup2(fd1[0], STDIN_FILENO);
	close(fd1[0]);
	dup2(fd2[1], STDOUT_FILENO);
	close(fd2[1]);
	execvp(cmd.front().c_str(), get_argv(cmd));
	ostringstream oss;
	oss << "Unknown command: [" + cmd.front() +  "]." << endl;
	string msg = oss.str();
	write(sockfd, msg.c_str(), msg.size());
	exit(EXIT_FAILURE);
    }
    else
    {
	close(fd1[0]);
	close(fd2[1]);
	string temp;
	if (inid == 0)
	{
	    string& in = user[sockfd - 4].output_deque.front();
	    write(fd1[1], in.c_str(), in.size());
	    close(fd1[1]);
	    temp = in;
	    user[sockfd - 4].output_deque.pop_front();
	    user[sockfd - 4].output_deque.emplace_back("");
	}
	else
	{
	    string& out = user[sockfd - 4].buffer[inid - 1];
	    write(fd1[1], out.c_str(), out.size());
	    close(fd1[1]);
	    out.clear();
	    inid = 0;
	}

	int status;
	wait(&status);
	cmd.clear();
	dup2(errfd, STDERR_FILENO);
	close(errfd);

	if (WEXITSTATUS(status) == EXIT_FAILURE)
	{
	    close(fd2[0]);
	    user[sockfd - 4].output_deque.push_front(temp);
	    user[sockfd - 4].output_deque.pop_back();
	    iss.str("");
	    return {};
	}
	char out[MAX_OUTPUT_SIZE];
	ssize_t n = read(fd2[0], &out, MAX_OUTPUT_SIZE);
	close(fd2[0]);
	out[n] = '\0';
	temp = out;
	if (pipenum > 0) user[sockfd - 4].output_deque[pipenum - 1] += temp;
	return temp;
    }
}

void broadcast(const string& msg)
{
    for(int i = 4; i <= maxfd; i++)
	if (FD_ISSET(i, &master))
	    write(i, msg.c_str(), msg.size());
}

void shell(int sockfd, string line)
{
    int inid = 0;

    line.erase(remove(line.begin(), line.end(), '\r'), line.end());
    line.erase(remove(line.begin(), line.end(), '\n'), line.end());
    string temp = replace_all(line, "| ", "|1 ");
    size_t pos = temp.find('<');
    if (pos != string::npos)
    {
	string sign = temp.substr(pos, 3);
	inid = stoi(sign.substr(1));
	ostringstream oss;
	if (user[sockfd - 4].buffer[inid - 1][0] == '\0')
	{
	    oss << "*** Error: the pipe #" << inid << "->#" << sockfd - 3 << " does not exist yet. ***" << endl;
	    write(sockfd, oss.str().c_str(), oss.str().size());
	    return;
	}
	oss << "*** " << user[sockfd - 4].name << " (#" << sockfd - 3 << ") just received from " << user[inid - 1].name
	    << " (#" << user[inid - 1].id << ") by '" << line << "' ***" << endl;
	broadcast(oss.str());
	temp.erase(pos, sign.size());
    }

    istringstream iss(temp);
    string s;
    iss >> s;
    if (s == "exit")
    {
	ostringstream oss;
	oss << "*** User '" << user[sockfd - 4].name << "' left. ***" << endl;
	broadcast(oss.str());
	user[sockfd - 4].id = 0;
	user[sockfd - 4].name = "(no name)";
	user[sockfd - 4].output_deque.clear();
	user[sockfd - 4].env.clear();
	for (auto& buffer : user[sockfd - 4].buffer) buffer.clear();
	FD_CLR(sockfd, &master);
	close(sockfd);
    }
    else if (s.empty());
    else if (s == "setenv")
    {
	string name, value;
	iss >> name >> value;
	user[sockfd - 4].env[name] = value;
    }
    else if (s == "printenv")
    {
	string name;
	iss >> name;
	ostringstream oss;
	oss << name << "=" << user[sockfd - 4].env[name] << endl;
	write(sockfd, oss.str().c_str(), oss.str().size());
    }
    else if (s == "tell")
    {
	int tellid;
	iss >> tellid;
	ostringstream oss;
	if (user[tellid - 1].id == 0)
	{
	    oss << "*** Error: user #" << tellid << " does not exist yet. ***" << endl;
	    write(sockfd, oss.str().c_str(), oss.str().size());
	}
	else
	{
	    string msg;
	    getline(iss, msg);
	    oss << "*** " << user[sockfd - 4].name << " told you ***:" << msg << endl;
	    write(tellid + 3, oss.str().c_str(), oss.str().size());
	}
    }
    else if (s == "yell")
    {
	string msg;
	getline(iss, msg);
	ostringstream oss;
	oss << "*** " << user[sockfd - 4].name << " yelled ***:" << msg << endl;
	broadcast(oss.str());
    }
    else if (s == "name")
    {
	string name;
	iss >> name;
	ostringstream oss;
	if (is_used(name, sockfd - 4))
	{
	    oss << "*** User '" << name << "' already exists. ***" << endl;
	    write(sockfd, oss.str().c_str(), oss.str().size());
	}
	else
	{
	    user[sockfd - 4].name = name;
	    oss << "*** User from " << user[sockfd - 4].ip << "/" << user[sockfd - 4].port
		<< " is named '" << user[sockfd - 4].name << "'. ***" << endl;
	    broadcast(oss.str());
	}
    }
    else if (s == "who")
    {
	string title = "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n";
	write(sockfd, title.c_str(), title.size());
	for (int i = 0; i < MAX_USER_NUM; i++)
	{
	    if (user[i].id > 0)
	    {
		ostringstream oss;
		oss << user[i].id << "\t" << user[i].name << "\t" << user[i].ip << "/" << user[i].port;
		if (i == sockfd - 4) oss << "\t<-me";
		oss << endl;
		write(sockfd, oss.str().c_str(), oss.str().size());
	    }
	}
    }
    else if (is_unknown(s))
    {
	user[sockfd - 4].output_deque.pop_front();
	user[sockfd - 4].output_deque.emplace_back("");
	ostringstream oss;
	oss << "Unknown command: [" + s +  "]." << endl;
	write(sockfd, oss.str().c_str(), oss.str().size());
    }
    else
    {
	vector<string> cmd = {s};
	while (iss >> s)
	{
	    if (s[0] == '|')
	    {
		int pipenum = stoi(s.substr(1));
		exec_pipe(sockfd, cmd, iss, pipenum, inid);
	    }
	    else if (s == ">")
	    {
		string filename;
		iss >> filename;
		ofstream fout(filename);
		if (!fout) cerr << "can't open file" << endl;
		fout << exec_pipe(sockfd, cmd, iss, 0, inid);
		fout.close();
	    }
	    else if (s[0] == '>')
	    {
		int outid = stoi(s.substr(1));
		ostringstream oss;
		if (user[outid - 1].id == 0)
		{
		    oss << "*** Error: user #" << outid << " does not exist yet. ***" << endl;
		    write(sockfd, oss.str().c_str(), oss.str().size());
		    return;
		}
		if (user[outid - 1].buffer[sockfd - 4][0] != '\0')
		{
		    oss << "*** Error: the pipe #" << sockfd - 3 << "->#" << outid << " already exists. ***" << endl;
		    write(sockfd, oss.str().c_str(), oss.str().size());
		    return;
		}
		oss << "*** " << user[sockfd - 4].name << " (#" << sockfd - 3 << ") just piped '" << line
		    << "' to " << user[outid - 1].name << " (#" << outid << ") ***" << endl;
		broadcast(oss.str());
		user[outid - 1].buffer[sockfd - 4] = exec_pipe(sockfd, cmd, iss, 0, inid);

	    }
	    else cmd.emplace_back(s);
	}

	if (!cmd.empty())
	{
	    string out = exec_pipe(sockfd, cmd, iss, 0, inid);
	    write(sockfd, out.c_str(), out.size());
	}
    }
}

void login(int sockfd)
{
    for (int i = 4; i <= maxfd; i++)
    {
	if (user[i - 4].id == 0)
	{
	    user[i - 4].id = i - 3;
	    user[i - 4].output_deque.resize(MAX_DEQUE_SIZE);
	    user[i - 4].env["PATH"] = "bin:.";
	    ostringstream oss;
	    oss << "*** User '" << user[i - 4].name << "' entered from "
		<< user[i - 4].ip << "/" << user[i - 4].port << ". ***" << endl;
	    write(sockfd, welcome.c_str(), welcome.size());
	    broadcast(oss.str());
	    write(sockfd, prompt.c_str(), prompt.size());
	    break;
	}
    }
}

int main(int argc, char* argv[])
{
    int newsockfd, clilen, childpid;;
    sockaddr_in cli_addr, serv_addr;
    timeval timeout = {};

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	cerr << "server: can't open stream socket" << endl;
    fcntl(sockfd, F_SETFL, O_NONBLOCK);
    
    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port        = htons(atoi(argv[1]));

    if (::bind(sockfd, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	cerr << "server: can't bind local address" << endl;

    listen(sockfd, 5);
    chdir("../ras/");

    FD_ZERO(&master);
    FD_ZERO(&read_fds);
    FD_SET(sockfd, &master);
    maxfd = sockfd;

    char line[MAX_LINE_SIZE];
    for( ; ; )
    {
	read_fds = master;
	timeout.tv_sec = 0;
	timeout.tv_usec = 10000;
	if (select(maxfd + 1, &read_fds, nullptr, nullptr, &timeout) < 0)
	    cerr << "server: select error" << endl;

	for(int i = 3; i <= maxfd; i++)
	{
	    if (FD_ISSET(i, &read_fds))
	    {
		if (i == sockfd)
		{
		    clilen = sizeof(cli_addr);
		    newsockfd = accept(sockfd, (sockaddr*)&cli_addr, (socklen_t*)&clilen);
		    if (newsockfd < 0) cerr << "server: accept error" << endl;
		    else
		    {
			FD_SET(newsockfd, &master);
			if (newsockfd > maxfd) maxfd = newsockfd;
			login(newsockfd);
		    }
		}
		else
		{
		    bzero(line, MAX_LINE_SIZE);
		    read(i, line, MAX_LINE_SIZE);
		    shell(i, line);
		    write(i, prompt.c_str(), prompt.size());
		}
	    }
	}
    }
}
