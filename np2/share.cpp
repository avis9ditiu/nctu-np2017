#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <signal.h>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <deque>
#include <fstream>
#define SHMKEY (key_t)2017
#define MAX_OUTPUT_SIZE 10000
#define MAX_DEQUE_SIZE  1000
#define MAX_USER_NUM    31
using namespace std;

vector<string> bin_cmd = {"ls", "cat", "removetag", "removetag0", "number", "noop", "delayedremovetag"};
deque <string> output_deque(MAX_DEQUE_SIZE);
struct User
{
    int  id;
    char name[32];
    char ip[16];
    int  port;
    int  pid;
    char buffer[MAX_USER_NUM][MAX_OUTPUT_SIZE];
};
User* user;
int shmid;
int myid = 0;

string replace_all(string s, string&& from, string&& to)
{
    for(size_t pos = 0; (pos = s.find(from, pos)) != string::npos; )
    {
	s.replace(pos, from.length(), to);
	pos += to.length();
    }
    return s;
}

bool is_unknown(string& cmd)
{
    return find(bin_cmd.begin(), bin_cmd.end(), cmd) == bin_cmd.end();
}

bool is_used(string& name, int request)
{
    for (int i = 1; i < MAX_USER_NUM; i++)
	if (user[i].id > 0 && i != request && strcmp(user[i].name, name.c_str()) == 0)
	    return true;
    return false;
}

char** get_argv(vector<string>& cmd)
{
    auto argv = new const char*[cmd.size() + 1];
    for (int i = 0; i < cmd.size(); i++)
	argv[i] = cmd[i].c_str();
    argv[cmd.size()] = nullptr;
    return (char**)argv;
}

bool checknull(char buffer[])
{
    for (int i = 0; i < MAX_OUTPUT_SIZE; i++)
	if (buffer[i] != '\0')
	    return false;
    return true;
}

void broadcast(const string msg)
{
    for (int i = 1; i < MAX_USER_NUM; i++)
	if (user[i].id > 0)
	{
	    while (!checknull(user[0].buffer[i]));
	    strcpy(user[0].buffer[i], msg.c_str());
	    kill(user[i].pid, SIGUSR1);
	}
}

void sig_handler(int sig)
{
    if (sig == SIGUSR1)
    {
	cout << user[0].buffer[myid] << endl;
	bzero(user[0].buffer[myid], MAX_OUTPUT_SIZE);
	signal(sig, sig_handler);
    }
    else if (sig == SIGINT)
    {
	shmctl(shmid, IPC_RMID, nullptr);
	exit(EXIT_SUCCESS);
    }
}

string exec_pipe(vector<string>& cmd, istringstream& iss, int pipenum, int& inid)
{
    int fd1[2],fd2[2];
    pipe(fd1);
    pipe(fd2);
    if (fork() == 0)
    {
	close(fd1[1]);
	close(fd2[0]);
	dup2(fd1[0], STDIN_FILENO);
	close(fd1[0]);
	dup2(fd2[1], STDOUT_FILENO);
	close(fd2[1]);
	execvp(cmd.front().c_str(), get_argv(cmd));
	cerr << "Unknown command: [" + cmd.front() + "]." << endl;
	exit(EXIT_FAILURE);
    }
    else
    {
	close(fd1[0]);
	close(fd2[1]);
	string temp;
	if (inid == 0)
	{
	    string& in = output_deque.front();
	    write(fd1[1], in.c_str(), in.size());
	    close(fd1[1]);
	    temp = in;
	    output_deque.pop_front();
	    output_deque.emplace_back("");
	}
	else
	{
	    write(fd1[1], user[myid].buffer[inid], MAX_OUTPUT_SIZE);
	    close(fd1[1]);
	    bzero(user[myid].buffer[inid], MAX_OUTPUT_SIZE);
	    inid = 0;
	}

	int status;
	wait(&status);
	cmd.clear();
	if (WEXITSTATUS(status) == EXIT_FAILURE)
	{
	    close(fd2[0]);
	    output_deque.push_front(temp);
	    output_deque.pop_back();
	    iss.str("");
	    return {};
	}
	char out[MAX_OUTPUT_SIZE];
	ssize_t n = read(fd2[0], &out, MAX_OUTPUT_SIZE);
	close(fd2[0]);
	out[n] = '\0';
	temp = out;
	if (pipenum > 0) output_deque[pipenum - 1] += temp;
	return temp;
    }
}

void shell(string& line)
{
    int inid = 0;

    string temp = replace_all(line, "| ", "|1 ");
    size_t pos = temp.find('<');
    if (pos != string::npos)
    {
	string sign = temp.substr(pos, 3);
	inid = stoi(sign.substr(1));
	if (user[myid].buffer[inid][0] == '\0')
	{
	    cout << "*** Error: the pipe #" << inid << "->#" << myid << " does not exist yet. ***" << endl;
	    return;
	}	
	ostringstream oss;
	oss << "*** " << user[myid].name << " (#" << myid << ") just received from " << user[inid].name
	    << " (#" << user[inid].id << ") by '" << line << "' ***";
	broadcast(oss.str());
	temp.erase(pos, sign.size());
    }

    istringstream iss(temp);
    string s;
    iss >> s;
    if (s == "exit")
    {
	ostringstream oss;
	oss << "*** User '" << user[myid].name << "' left. ***";
	broadcast(oss.str());
	user[myid].id = 0;
	for (int i = 1; i < MAX_USER_NUM; i++)
	    bzero(user[myid].buffer[i], MAX_OUTPUT_SIZE);
	shmdt(user);
	exit(EXIT_SUCCESS);
    }
    else if (s.empty());
    else if (s == "setenv")
    {
	string name, value;
	iss >> name >> value;
	if (setenv(name.c_str(), value.c_str(), 1) < 0)
	    cerr << "setenv error" << endl;
    }
    else if (s == "printenv")
    {
	string name;
	iss >> name;
	char* value = getenv(name.c_str());
	if (value == nullptr) cerr << "printenv error" << endl;
	cout << name << "=" << value << endl;
    }
    else if (s == "tell")
    {
	int tellid;
	iss >> tellid;
	if (user[tellid].id == 0)
	    cout << "*** Error: user #" << tellid << " does not exist yet. ***" << endl;
	else
	{
	    string msg;
	    getline(iss, msg);
	    ostringstream oss;
	    oss << "*** " << user[myid].name << " told you ***:" << msg;
	    strcpy(user[0].buffer[tellid], oss.str().c_str());
	    kill(user[tellid].pid, SIGUSR1);
	}
    }
    else if (s == "yell")
    {
	string msg;
	getline(iss, msg);
	ostringstream oss;
	oss << "*** " << user[myid].name << " yelled ***:" << msg;
	broadcast(oss.str());
    }
    else if (s == "name")
    {
	string name;
	iss >> name;
	if (is_used(name, myid))
	    cout << "*** User '" << name << "' already exists. ***" << endl;
	else
	{
	    strcpy(user[myid].name, name.c_str());
	    ostringstream oss;
	    oss << "*** User from " << user[myid].ip << "/" << user[myid].port
		<< " is named '" << user[myid].name << "'. ***";
	    broadcast(oss.str());
	}
    }
    else if (s == "who")
    {
	string title = "<ID>\t<nickname>\t<IP/port>\t<indicate me>";
	cout << title << endl;
	for (int i = 1; i < MAX_USER_NUM; i++)
	{
	    if (user[i].id > 0)
	    {
		cout << user[i].id << "\t" << user[i].name << "\t" << user[i].ip << "/" << user[i].port;
		if (i == myid) cout << "\t<-me";
		cout << endl;
	    }
	}
    }
    else if (is_unknown(s))
    {
	output_deque.pop_front();
	output_deque.emplace_back("");
	cerr << "Unknown command: [" + s +  "]." << endl;
    }
    else
    {
	vector<string> cmd = {s};
	while (iss >> s)
	{
	    if (s[0] == '|')
	    {
		int pipenum = stoi(s.substr(1));
		exec_pipe(cmd, iss, pipenum, inid);
	    }
	    else if (s == ">")
	    {
		string filename;
		iss >> filename;
		ofstream fout(filename);
		if (!fout) cerr << "can't open file" << endl;
		fout << exec_pipe(cmd, iss, 0, inid);
		fout.close();
	    }
	    else if (s[0] == '>')
	    {
		int outid = stoi(s.substr(1));
		if (user[outid].id == 0)
		{
		    cout << "*** Error: user #" << outid << " does not exist yet. ***" << endl;
		    return;
		}
		if (user[outid].buffer[myid][0] != '\0')
		{
		    cout << "*** Error: the pipe #" << myid << "->#" << outid << " already exists. ***" << endl;
		    return;
		}
		ostringstream oss;
		oss << "*** " << user[myid].name << " (#" << myid << ") just piped '" << line
		    << "' to " << user[outid].name << " (#" << outid << ") ***";
		broadcast(oss.str());
		strcpy(user[outid].buffer[myid], exec_pipe(cmd, iss, 0, inid).c_str());
	    }
	    else cmd.emplace_back(s);
	}

	if (!cmd.empty()) cout << exec_pipe(cmd, iss, 0, inid);
    }
}

void create_shm()
{
    shmid = shmget(SHMKEY, sizeof(User) * MAX_USER_NUM, IPC_CREAT | 0666);
    if (shmid < 0) cerr << "server: shmget error" << endl;
    if ((user = (User*)shmat(shmid, nullptr, 0)) == (User*)(-1))
	cerr << "server: shmat error" << endl;
    bzero(user, sizeof(User) * MAX_USER_NUM);
    signal(SIGINT, sig_handler);
    shmdt(user);
}

void add_user()
{
    for (int i = 1; i < MAX_USER_NUM; i++)
    {
	if (user[i].id == 0)
	{
	    myid = i;
	    user[i].id = i;
	    strcpy(user[i].name, "(no name)");
	    strcpy(user[i].ip, "CGILAB");
	    user[i].port = 511;
	    user[i].pid = getpid();
	    ostringstream oss;
	    oss << "*** User '" << user[i].name << "' entered from "
		<< user[i].ip << "/" << user[i].port << ". ***";
	    broadcast(oss.str());
	    break;
	}
    }
}

void initialize(int sockfd)
{
    dup2(sockfd, STDIN_FILENO);
    dup2(sockfd, STDOUT_FILENO);
    dup2(sockfd, STDERR_FILENO);

    shmid = shmget(SHMKEY, sizeof(User) * MAX_USER_NUM, 0666);
    if (shmid < 0) cerr << "server: shmget error" << endl;
    if ((user = (User*)shmat(shmid, nullptr, 0)) == (User*)(-1))
	cerr << "server: shmat error" << endl;

    setenv("PATH", "bin:.", 1);
    chdir("../ras/");
    signal(SIGUSR1, sig_handler);

    cout << "****************************************" << endl;
    cout << "** Welcome to the information server. **" << endl;
    cout << "****************************************" << endl;
    add_user();
}

int main(int argc, char* argv[])
{
    int sockfd, newsockfd, clilen, childpid;;
    sockaddr_in cli_addr, serv_addr;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	cerr << "server: can't open stream socket" << endl;

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port        = htons(atoi(argv[1]));

    if (::bind(sockfd, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	cerr << "server: can't bind local address" << endl;

    listen(sockfd, 5);
    create_shm();    

    for ( ; ; )
    {
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (sockaddr*)&cli_addr, (socklen_t*)&clilen);
	if (newsockfd < 0) cerr << "server: accept error" << endl;
	if ((childpid = fork()) < 0) cerr << "server: fork error" << endl;
	else if (childpid == 0)
	{
	    close(sockfd);
	    initialize(newsockfd);

	    string line;
	    for ( ; ; )
	    {
		cout << "% ";
		getline(cin, line);
		shell(line);
	    }
	}
	close(newsockfd);
    }
}
