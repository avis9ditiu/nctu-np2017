#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <netdb.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <string>
#include <array>
#include <regex>
#define MAX_BUF_SIZE 500000
using namespace std;

fd_set read_fds, master;
struct Host
{
    sockaddr_in	sin;
    int		sock;
    FILE*       fin;
    bool	received;
};

void replace_all(string& s, string&& from, string&& to)
{
    for(size_t pos = 0; (pos = s.find(from, pos)) != string::npos; )
    {
	s.replace(pos, from.length(), to);
	pos += to.length();
    }
}

void add_host(Host& host, const string& name, int port, const string& file)
{
    bzero(&host.sin, sizeof(host.sin));
    host.sin.sin_family = AF_INET;
    host.sin.sin_port = htons(port);
    hostent* he = gethostbyname(name.c_str());
    host.sin.sin_addr = *((in_addr*)he->h_addr);

    if ((host.sock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
	cerr << "failed to build a socket" << endl;
    if ((host.fin = fopen(file.c_str(), "r")) == nullptr)
	cerr << "failed to open file" << endl;

    FD_SET(fileno(host.fin), &master);
    fcntl(host.sock, F_SETFL, O_NONBLOCK);

    connect(host.sock, (sockaddr*)&host.sin, sizeof(host.sin));
    FD_SET(host.sock, &master);
    host.received = false;
}

void initialize(array<Host, 5>& hosts)
{
    string qstr, method;
    int i, port;
    string name, file;

    method = getenv("REQUEST_METHOD");
    if (method == "GET") qstr = getenv("QUERY_STRING");

    istringstream iss(qstr);
    string part;
    smatch sm;
    regex name_re("h(.+)=(.+)"), port_re("p(.+)=(.+)"), file_re("f(.+)=(.+)");
    while (getline(iss, part, '&'))
    {
	if (regex_match(part, sm, name_re)) name = sm[2];
	else if (regex_match(part, sm, port_re)) port = stoi(sm[2]);
	else if (regex_match(part, sm, file_re))
	{
	    i = stoi(sm[1]);
	    file = sm[2];
	    add_host(hosts[i - 1], name, port, file);
	}
    }
}

void display_head(array<Host, 5>& hosts)
{
    cout << "Content-Type: text/html" << endl << endl;
    cout << "<html>" << endl;
    cout << "<head>" << endl;
    cout << "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" << endl;
    cout << "<title>Network Programming Homework 3</title>" << endl;
    cout << "</head>" << endl;
    cout << "<body bgcolor=#336699>" << endl;
    cout << "<font face=\"Courier New\" size=2 color=#FFFF99>" << endl;	
    cout << "<table width=\"800\" border=\"1\">" << endl;	
    cout << "<tr>" << endl;
    for (int i = 0; i < 5; i++) 
    {
	cout << "<td>";
	if (hosts[i].sock) cout << inet_ntoa(hosts[i].sin.sin_addr);
	cout << "</td>" << endl;
    }
    cout << "</tr>" << endl;
    cout << "<tr>" << endl;
    for (int i = 0; i < 5; i++) cout << "<td valign=\"top\" id=\"m" << i << "\"></td>" << endl;
    cout << "</tr>" << endl;
    cout << "</table>" << endl;
}

void display_tail()
{
    cout << "</font>" << endl;
    cout << "</body>" << endl;
    cout << "</html>" << endl;
}

void send_cmd(array<Host, 5>& hosts, int i)
{
    char cmd[MAX_BUF_SIZE];
    if (fgets(cmd, MAX_BUF_SIZE, hosts[i].fin) != nullptr)
    { 
	write(hosts[i].sock, cmd, strlen(cmd));
	strtok(cmd, "\r\n");
	cout << "<script>document.all['m" << i << "'].innerHTML += \"<b>" << cmd << "</b><br>\";</script>" << endl;
    }
    hosts[i].received = false;
}

void rm_host(Host& host)
{
    FD_CLR(fileno(host.fin), &master);
    fclose(host.fin);
    FD_CLR(host.sock, &master);
    close(host.sock);
    bzero(&host, sizeof(host));
}

void recv_out(array<Host, 5>& hosts, int i)
{
    char buffer[MAX_BUF_SIZE];
    ssize_t n = read(hosts[i].sock, buffer, MAX_BUF_SIZE);
    if (n == 0)
	rm_host(hosts[i]);
    else
    {
	buffer[n] = 0;
	istringstream iss(buffer);
	string line;
	while (getline(iss, line))
	{
	    line.erase(remove(line.begin(), line.end(), '\r'), line.end());	
	    replace_all(line, "&", "&amp;");
	    replace_all(line, "<", "&lt;");
	    replace_all(line, ">", "&gt;");
	    replace_all(line, "\"", "&quot;");
	    if (line.find("% ") != string::npos) hosts[i].received = true;
	    if (line != "% ") line += "<br>";
	    cout << "<script>document.all['m" << i << "'].innerHTML += \"" << line << "\";</script>" << endl;
	}
    }
}

int main()
{
    int	maxfd = getdtablesize();
    array<Host, 5> hosts = {};
    timeval timeout = {};

    FD_ZERO(&master);
    initialize(hosts);
    display_head(hosts);

    int i = 0;
    while (hosts[0].sock + hosts[1].sock + hosts[2].sock + hosts[3].sock + hosts[4].sock)
    {
	if (hosts[i].sock > 0)
	{
	    read_fds = master;
	    timeout.tv_sec = 0;
	    timeout.tv_usec = 10000;
	    if (select(maxfd, &read_fds, nullptr, nullptr, &timeout) < 0)
		cerr << "select failed" << endl;
	    if (hosts[i].received && FD_ISSET(fileno(hosts[i].fin), &read_fds))
		send_cmd(hosts, i);
	    if (FD_ISSET(hosts[i].sock, &read_fds))
		recv_out(hosts, i);
	}
	usleep(10000);
	i = (i + 1) % 5;
    }
    display_tail();
}
