#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <string>
using namespace std;
const char* path = "/u/gcs/106/0656136/public_html";

void http()
{
    string line, method, target, qstr, version;
    getline(cin, line);
    istringstream iss(line);
    iss >> method >> target >> version;
    size_t pos = target.find('?');
    if (pos != string::npos)    
    {
	qstr = target.substr(pos + 1);
	target = target.substr(0, pos);
    }
    target.insert(0, path);

    clearenv();
    setenv("REDIRECT_STATUS", "200", 1);
    setenv("DOCUMENT_ROOT", path, 1);
    setenv("REQUEST_METHOD", method.c_str(), 1);
    setenv("SCRIPT_FULLNAME", target.c_str(), 1);
    setenv("QUERY_STRING", qstr.c_str(), 1);
    setenv("SERVER_PROTOCOL", version.c_str(), 1);

    if (access(target.c_str(), F_OK|X_OK) != -1)
    {
	cout << version << " 200 OK" << endl;
	execl(target.c_str(), target.c_str(), nullptr);
    }
    else if (access(target.c_str(), F_OK|R_OK) != -1)
    {
	cout << version << " 200 OK" << endl;
	ifstream fin(target);
	cout << "Content-Type: text/html" << endl << endl;
	while (fin >> line) cout << line << endl;
	fin.close();
    }
    else
    {
	cout << version << " 404 Not Found" << endl;
	cout << "Content-Type: text/html" << endl << endl;
	cout << "<html>" << endl;
	cout << "<head><title>404 Not Found</title></head>" << endl;
	cout << "<body><h1>404 Not Found</h1></body>" << endl;
	cout << "</html>" << endl;
    }
}

int main(int argc, char* argv[])
{
    int sockfd, newsockfd, clilen, childpid;
    sockaddr_in cli_addr, serv_addr;

    chdir(path);
    signal(SIGCHLD, SIG_IGN);

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	cerr << "server: can't open stream socket" << endl;

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port        = htons(atoi(argv[1]));

    if (::bind(sockfd, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	cerr << "server: can't bind local address" << endl;

    listen(sockfd, 5);

    for ( ; ; )
    {
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (sockaddr*)&cli_addr, (socklen_t*)&clilen);
	if (newsockfd < 0) cerr << "server: accept error" << endl;
	if ((childpid = fork()) < 0) cerr << "server: fork error" << endl;
	else if (childpid == 0)
	{
	    close(sockfd);
	    dup2(newsockfd, STDIN_FILENO);  
	    dup2(newsockfd, STDOUT_FILENO);
	    close(newsockfd);
	    http();
	    exit(0);
	}
	close(newsockfd);
    }
}
