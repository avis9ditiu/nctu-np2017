#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <deque>
#include <algorithm>
#include <cstring>

#define MAX_OUTPUT_SIZE 100000
#define MAX_DEQUE_SIZE  1000
using namespace std;

vector<string> all_cmd = {"ls", "cat", "removetag", "removetag0", "number", "noop", "delayedremovetag"};
deque<string> output_deque(MAX_DEQUE_SIZE);

void replace_all(string& s, string&& from, string&& to)
{
    for(size_t pos = 0; (pos = s.find(from, pos)) != string::npos; )
    {
	s.replace(pos, from.length(), to);
	pos += to.length();
    }
}

bool is_unknown(const string& cmd)
{
    return find(all_cmd.begin(), all_cmd.end(), cmd) == all_cmd.end();
}

char** get_argv(vector<string>& cmd)
{
    auto argv = new const char*[cmd.size() + 1];
    for (int i = 0; i < cmd.size(); i++)
	argv[i] = cmd[i].c_str();
    argv[cmd.size()] = nullptr;
    return (char**)argv;
}

string exec_pipe(vector<string>& cmd, int pipenum, istringstream& iss)
{
    int fd1[2],fd2[2], pid;
    pipe(fd1);
    pipe(fd2);
    if ((pid = fork()) == 0)
    {
	close(fd1[1]);
	close(fd2[0]);
	dup2(fd1[0], STDIN_FILENO);
	close(fd1[0]);
	dup2(fd2[1], STDOUT_FILENO);
	close(fd2[1]);
	execvp(cmd.front().c_str(), get_argv(cmd));
	cerr << "Unknown command: [" + cmd.front() + "]." << endl;
	exit(EXIT_FAILURE);
    }
    else
    {
	close(fd1[0]);
	close(fd2[1]);
	string& out = output_deque.front();
	write(fd1[1], out.c_str(), out.size());
	close(fd1[1]);
	string temp(output_deque.front().c_str());	
	output_deque.pop_front();
	output_deque.push_back("");

	int status;
	wait(&status);
	cmd.clear();
	if (WEXITSTATUS(status) == EXIT_FAILURE)
	{
	    close(fd2[0]);
	    output_deque.push_front(temp);
	    output_deque.pop_back();
	    iss.str("");
	    return {};
	}
	char buffer[MAX_OUTPUT_SIZE];
	ssize_t n = read(fd2[0], &buffer, MAX_OUTPUT_SIZE);
	close(fd2[0]);
	buffer[n] = '\0';
	temp = buffer;
	if (pipenum > 0) output_deque[pipenum - 1] += temp;
	return temp;
    }
}

void shell(string& line)
{   
    replace_all(line, "| ", "|1 ");

    istringstream iss(line);
    string s;
    iss >> s;
    if (s == "exit") exit(0);
    else if (s.empty());
    else if (s == "setenv")
    {
	string name, value;
	iss >> name >> value;
	if (setenv(name.c_str(), value.c_str(), 1) < 0)
	    cerr << "setenv error" << endl;
    }
    else if (s == "printenv")
    {
	string name;
	iss >> name;
	char* value = getenv(name.c_str());
	if (value == nullptr) cerr << "printenv error" << endl;
	cout << name << "=" << value << endl;
    }
    else if (is_unknown(s))
    {	
	output_deque.pop_front();
	output_deque.push_back("");
	cerr << "Unknown command: [" + s +  "]." << endl;
    }
    else 
    {
	vector<string> cmd = {s};
	while (iss >> s)
	{   
	    if (s[0] == '|')
	    {
		int pipenum = stoi(s.substr(1));
		exec_pipe(cmd, pipenum, iss);
	    }
	    else if (s == ">")
	    {  
		string filename;
		iss >> filename;
		ofstream fout(filename);
		if (!fout) cerr << "can't open file" << endl;
		fout << exec_pipe(cmd, 0, iss);
		fout.close();
	    }
	    else cmd.emplace_back(s);
	}
	if (!cmd.empty()) cout << exec_pipe(cmd, 0, iss);
    }
}

int main(int argc, char* argv[])
{
    int sockfd, newsockfd, clilen, childpid;;
    sockaddr_in cli_addr, serv_addr;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	cerr << "server: can't open stream socket" << endl;

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port        = htons(atoi(argv[1]));

    if (::bind(sockfd, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	cerr << "server: can't bind local address" << endl;

    listen(sockfd, 5);

    for ( ; ; )
    {
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (sockaddr*)&cli_addr, (socklen_t*)&clilen);
	if (newsockfd < 0) cerr << "server: accept error" << endl;
	if ((childpid = fork()) < 0) cerr << "server: fork error" << endl;
	else if (childpid == 0)
	{
	    dup2(newsockfd, STDIN_FILENO);
	    dup2(newsockfd, STDOUT_FILENO);
	    dup2(newsockfd, STDERR_FILENO);
	    close(sockfd);

	    setenv("PATH", "bin:.", 1);
	    chdir("../ras/");
	    cout << "****************************************" << endl;
	    cout << "** Welcome to the information server. **" << endl;
	    cout << "****************************************" << endl;

	    string line;
	    for ( ; ; )
	    {
		cout << "% ";
		getline(cin, line);
		shell(line);
	    }
	}
	close(newsockfd);
    }
}
